const express = require("express");
const router = express.Router();

const app = express();

const ProductController = require('./app/controllers/ProductController');
const AuthController = require('./app/controllers/AuthController');

router.post("/users", AuthController.store);
router.post("/auth/authenticate", AuthController.login);

const authMiddleware = require('./app/middleware/auth');
router.use(authMiddleware);

router.get("/users", AuthController.index);
router.get("/users/:id", AuthController.show);
router.put("/users/:id", AuthController.update);
router.delete("/users/:id", AuthController.destroy);

router.get("/products", ProductController.index);
router.get("/products/:id", ProductController.show);
router.post("/products", ProductController.store);
router.put("/products/:id", ProductController.update);
router.delete("/products/:id", ProductController.destroy);

module.exports = router;