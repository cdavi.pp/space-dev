const mongoose = require('mongoose');
const express = require("express")
const router = express.Router();

const Product = mongoose.model('Product');

const authMiddleware = require('../middleware/auth');
router.use(authMiddleware);

module.exports = {
    async index(req, res){
        const {page = 1} = req.query;
        const products = await Product.paginate({},{ page, limit: 7});

        return res.json(products);
    },

    async show(req, res){
        const product = await Product.findById(req.params.id);

        return res.json(product);
    },
    
    async store(req, res){
        try {
            const product = await Product.create(req.body);
            return res.json(product);
        } catch (error) {
            return res.status(400).send({ error: 'Insert Product failed.' });
        }  
    },

    async update(req, res){
        const product = await Product.findByIdAndUpdate(req.params.id, req.body, {new: true});

        return res.json(product);
    },

    async destroy(req, res){
        await Product.findByIdAndRemove(req.params.id);

        return res.send();
    }
};