const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const authConfig = require('../../config/auth');

const User = mongoose.model('User');

function gererateToken(params = {}) {
    return jwt.sign(params, authConfig.secret, {
        expiresIn: 86400,
    });
}

module.exports = {
    async index(req, res){
        const {page = 1} = req.query;
        const users = await User.paginate({},{ page, limit: 7});

        return res.json(users);
    },

    async show(req, res){
        const user = await User.findById(req.params.id);
        return res.json(user);
    },
    
    async store(req, res){
        const { email } = req.body;

        try {
            if (await User.findOne({ email })) {
                return res.status(400).send({ error: 'User already exists' });
            }

            const user = await User.create(req.body);
            user.password = undefined;

            return res.send({ 
                user, 
                token: gererateToken({ id: user.id }), 
            });
        } catch (error) {
            return res.status(400).send({ error: 'Insert User failed.' });
        }
    },

    async login(req, res){
        const { email, password } = req.body;

        const user = await User.findOne({ email }).select('+password');

        if (!user)
            return res.status(400).send({ error: 'User not found' });

        if (!await bcrypt.compare(password, user.password))
            return res.status(400).send({ error: 'Invalid password' });

        user.password = undefined;

        const token = jwt.sign({ id: user.id }, authConfig.secret, {
            expiresIn: 86400,
        });

        res.send({ 
            user, 
            token: gererateToken({ id: user.id }), 
        });
    },

    async update(req, res){
        const user = await User.findByIdAndUpdate(req.params.id, req.body, {new: true})
        return res.json(user);
    },

    async destroy(req, res){
        await User.findByIdAndRemove(req.params.id);
        return res.send();
    }
};