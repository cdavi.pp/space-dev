const express = require('express');
const mongoose = require('mongoose');
const requireDir = require('require-dir');
const cors = require('cors');
const port = 3000;

const app = express();
app.use(express.json());
app.use(cors());

mongoose.connect('mongodb://localhost:27017/space-dev-api', { 
    useUnifiedTopology: true, 
    useNewUrlParser: true ,
    useCreateIndex: true, // P/ poder criar registros.
});

requireDir('./src/app/models');

app.get('/', (rep, res)=> {
    res.send('API - Nodejs(Space - dev)');
});
app.use("/api", require('./src/routes'));

app.listen(port, function(){
    console.log('Server started at port: ' + port);
});